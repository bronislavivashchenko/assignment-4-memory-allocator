#define _DEFAULT_SOURCE

#include <assert.h>
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"

#ifndef HEAP_SIZE
#define HEAP_SIZE 8175
#endif
#ifndef BLOCK_SIZE
#define BLOCK_SIZE 50
#endif

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void test1() {
    printf("Test 1\n");
    void* heap = heap_init(HEAP_SIZE);

    assert(heap && "Test failed: Heap initialization error");

    debug_heap(stdout, heap);

    printf("Allocating 2 blocks:\n");
    void* block1 = _malloc(BLOCK_SIZE);
    void* block2 = _malloc(BLOCK_SIZE);
    debug_heap(stdout, heap);

    assert(block1 && "Test failed: Block 1 allocation error");
    assert(block2 && "Test failed: Block 2 allocation error");

    printf("Freeing 2 blocks:\n");
    _free(block2);
    _free(block1);
    debug_heap(stdout, heap);

    assert(block_get_header(block1)->is_free && "Test failed: Block 1 should be free");
    assert(block_get_header(block2)->is_free && "Test failed: Block 2 should be free");

    heap_term();
    printf("Test 1 passed\n\n");
}

void test2() {
    printf("Test 2\n");
    void* heap = heap_init(HEAP_SIZE);

    assert(heap && "Test failed: Heap initialization error");

    debug_heap(stdout, heap);

    printf("Allocating 3 blocks:\n");
    void* block1 = _malloc(BLOCK_SIZE);
    void* block2 = _malloc(BLOCK_SIZE);
    void* block3 = _malloc(BLOCK_SIZE);
    debug_heap(stdout, heap);

    printf("Freeing the second block:\n");
    _free(block2);
    debug_heap(stdout, heap);

    assert(block_get_header(block2)->is_free && "Test failed: block 2 should be free");
    assert(!block_get_header(block1)->is_free && "Test failed: block 1 should not be free");
    assert(!block_get_header(block3)->is_free && "Test failed: block 3 should not be free");

    printf("Freeing the first and third blocks:\n");
    _free(block1);
    _free(block3);
    debug_heap(stdout, heap);

    assert(block_get_header(block1)->is_free && "Test failed: Block 1 should be free");
    assert(block_get_header(block3)->is_free && "Test failed: Block 3 should be free");

    heap_term();
    printf("Test 2 passed\n\n");
}

void test3() {
    printf("Test 3\n");
    void* heap = heap_init(HEAP_SIZE);

    assert(heap && "Test failed: Heap initialization error");

    debug_heap(stdout, heap);

    printf("Allocating 3 blocks:\n");
    void* block1 = _malloc(BLOCK_SIZE);
    void* block2 = _malloc(BLOCK_SIZE);
    void* block3 = _malloc(BLOCK_SIZE);
    debug_heap(stdout, heap);

    printf("Freeing the first and second blocks:\n");
    _free(block2);
    _free(block1);
    debug_heap(stdout, heap);

    assert(block_get_header(block2)->is_free && "Test failed: Block 2 should be free");
    assert(block_get_header(block1)->is_free && "Test failed: Block 1 should be free");
    assert(block_get_header(block1)->capacity.bytes == BLOCK_SIZE + size_from_capacity((block_capacity){ BLOCK_SIZE }).bytes
        && "Test failed: Block 1 is not merged with the next block");

    printf("Freeing the third block:\n");
    _free(block3);
    debug_heap(stdout, heap);

    assert(block_get_header(block3)->is_free && "Test failed: Block 3 should be free");

    heap_term();
    printf("Test 3 passed\n\n");
}

void test4() {
    printf("Test 4\n");
    void* heap = heap_init(HEAP_SIZE);

    assert(heap && "Test failed: Heap initialization error");

    debug_heap(stdout, heap);

    printf("Allocating 1 block:\n");
    void* block1 = _malloc(HEAP_SIZE + BLOCK_SIZE);
    debug_heap(stdout, heap);

    assert(block1 && "Test failed: Block allocation error");

    printf("Freeing 1 block:\n");
    _free(block1);
    debug_heap(stdout, heap);

    assert(block_get_header(block1)->is_free && "Test failed: Block should be free");

    heap_term();
    printf("Test 4 passed\n\n");
}

void test5() {
    printf("Test 5\n");
    void* heap = heap_init(HEAP_SIZE);

    assert(heap && "Test failed: Heap initialization error");

    void* addr = mmap( heap, HEAP_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS , -1, 0);
    assert(addr != MAP_FAILED && "Test failed: Address mapping error");
    debug_heap(stdout, heap);

    printf("Allocating 1 block:\n");
    void* block1 = _malloc(HEAP_SIZE + BLOCK_SIZE);
    debug_heap(stdout, heap);

    assert(block1 && "Test failed: Block allocation error");

    printf("Freeing 1 block:\n");
    _free(block1);
    debug_heap(stdout, heap);

    assert(block_get_header(block1)->is_free && "Test failed: Block should be free");

    heap_term();
    printf("Test 5 passed\n\n");
}

int main() {
    test1();
    test2();
    test3();
    test4();
    test5();

    printf("Tests passed.\n");
}